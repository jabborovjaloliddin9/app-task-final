package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Three {

    static Map<Character, Character> map = new HashMap<>();

    public static void main(String[] args) {
        map.put('r', 's');
        map.put('o', 'l');
        map.put('b', 'c');
        map.put('n', 'k');
        map.put('t', 'e');
        map.put('e', 'p');
        map.put('s', 't');
        map.put('d', 'o');
        map.put('a', 't');
        map.put('u', 'f');
        map.put('i', 'f');
        map.put('f', 'e');
        map.put('l', 'a');
        map.put('k', 'p');
        map.put('c', 'd');

        Scanner scanner = new Scanner(System.in);
        Map<Character, Character> harflar = new HashMap<>();
        System.out.println("So`zni kiriting: ");
        String str = scanner.nextLine();
        System.out.println(shifrlash(str));
    }

    static String shifrlash(String str){
        String result = "";
        for (int i = 0; i < str.length(); i++) {
               result +=map.get(str.charAt(i)).toString();
        }
        return "Shifrlangan matn: " + result;
    }

}
