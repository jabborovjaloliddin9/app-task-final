package com.company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // 1 -masala

        Scanner scanner =new Scanner(System.in);
        int[][] massiv = new int[7][7];
        //o`rtasi
        int x0 = massiv.length / 2;
        int y0 = massiv[0].length / 2;
            int count = 0;
            System.out.println("Robotning boshlang'ich koordinatasini kiriting:");
            System.out.print("Gorizontal: "); int x = scanner.nextInt();
            if (x < 0 || x >= massiv.length) {
                System.out.println("[0;7) oraliqda kiriting");
                return;
            }
            System.out.print("Vertikal: "); int y = scanner.nextInt();
            if (y < 0 || y >= massiv.length) {
                System.out.println("[0;7) oraliqda kiriting");
                return;
            }
            if (x > x0) count = ortga(x, x0);
            else count = oldinga(x, x0);
            if (y > y0) count+= ortga(y, y0);
            else count +=oldinga(y, y0);
            System.out.println(count);
    }

    static int oldinga(int current, int middle){
        int count = 0;
        while (current != middle){
            current++;
            count++;
        }
        return count;
    }

    static int ortga(int current, int middle){
        int count = 0;
        while (current != middle){
            current--;
            count++;
        }
        return count;
    }
}
