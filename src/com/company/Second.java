package com.company;

import java.util.Scanner;

public class Second {

    public static void main(String[] args) {

        // 2 - masala
        Scanner scanner = new Scanner(System.in);

        System.out.print("n sonini kiriting: ");
        int n = scanner.nextInt();
        int kopaytma = 1;
        for (int i = 2; i <= n; i++) {
            int j = 2;
            while (j < Math.sqrt(i) && i % j != 0) {
                j++;
            }
            if (j > Math.sqrt(i)) kopaytma *= i;
        }
        int nollar = 0;
        while (kopaytma % 10 == 0) {
            nollar++;
            kopaytma = kopaytma / 10;
        }
        System.out.println(nollar);
    }
}
